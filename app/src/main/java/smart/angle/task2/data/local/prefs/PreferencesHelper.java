package smart.angle.task2.data.local.prefs;

public interface PreferencesHelper {

    int getLastIndex();

    void setLastIndex(int index);

}
