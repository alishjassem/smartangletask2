package smart.angle.task2.ui.main;

public interface MainNavigator {

    void handleError(Throwable throwable);

    void onSignUp(String name, String email, String password);

    void openGallery();
}
