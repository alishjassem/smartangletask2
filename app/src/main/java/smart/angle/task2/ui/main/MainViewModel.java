package smart.angle.task2.ui.main;

import android.text.Editable;

import smart.angle.task2.data.DataManager;
import smart.angle.task2.ui.base.BaseViewModel;
import smart.angle.task2.utils.StringRuleUtil;
import smart.angle.task2.utils.rx.SchedulerProvider;


public class MainViewModel extends BaseViewModel<MainNavigator> {

    private static final String TAG = "MainViewModel";

    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }

    public void signUp(Editable editableName, Editable editableEmail, Editable editablePassword){
        if (StringRuleUtil.EMAIL_RULE.validate(editableEmail) |
                StringRuleUtil.NOT_EMPTY_RULE.validate(editableName) |
                StringRuleUtil.PASSWORD_RULE.validate(editablePassword)) {
            return;
        }

        getNavigator().onSignUp(editableName.toString(), editableEmail.toString(), editablePassword.toString());
    }
}
