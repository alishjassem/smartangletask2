package smart.angle.task2.di.builder;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import smart.angle.task2.ui.main.MainActivity;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector()
    abstract MainActivity bindMainActivity();
}
