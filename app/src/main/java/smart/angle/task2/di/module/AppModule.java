package smart.angle.task2.di.module;

import android.app.Application;
import android.content.Context;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import smart.angle.task2.BuildConfig;
import smart.angle.task2.data.AppDataManager;
import smart.angle.task2.data.DataManager;
import smart.angle.task2.data.local.prefs.AppPreferencesHelper;
import smart.angle.task2.data.local.prefs.PreferencesHelper;
import smart.angle.task2.di.ApiInfo;
import smart.angle.task2.di.DatabaseInfo;
import smart.angle.task2.di.PreferenceInfo;
import smart.angle.task2.utils.AppConstants;
import smart.angle.task2.utils.rx.AppSchedulerProvider;
import smart.angle.task2.utils.rx.SchedulerProvider;

@Module
public class AppModule {

    @Provides
    @ApiInfo
    String provideApiKey() {
        return BuildConfig.API_KEY;
    }

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

}
