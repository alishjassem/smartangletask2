package smart.angle.task2.utils;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;

import androidx.databinding.BindingAdapter;
import smart.angle.task2.R;

public final class BindingUtils {

    private BindingUtils() {
        // This class is not publicly instantiable
    }

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Glide.with(context).load(url).into(imageView);
    }

    @BindingAdapter({"validation", "errorMsg"})
    public static void setErrorEnable(TextInputLayout textInputLayout, StringRuleUtil.StringRule stringRule, final String errorMsg) {

        if (textInputLayout.getEditText() != null)

            textInputLayout.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String text = textInputLayout.getEditText().getText().toString();
                    String notEmpty = textInputLayout.getContext().getString(R.string.field_can_not_be_empty);

                    if (text.isEmpty())
                        textInputLayout.setError(notEmpty);
                    else if (stringRule.validate(textInputLayout.getEditText().getText()))
                        textInputLayout.setError(errorMsg);
                    else
                        textInputLayout.setError(null);
                }
            });
    }

}
