package smart.angle.task2.utils;

public final class AppConstants {

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;

    public static final String DB_NAME = "smartangle.db";

    public static final long NULL_INDEX = -1L;

    public static final String PREF_NAME = "smartangle";

    public static final int REQ_READ_EXTERNAL_STORAGE = 123;

    public static final int REQ_CODE_PICK_IMAGE = 1234;

    public static final String STATUS_CODE_FAILED = "failed";

    public static final String STATUS_CODE_SUCCESS = "success";

    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
