package smart.angle.task2.utils;

import android.text.Editable;
import android.text.TextUtils;
import android.util.Patterns;

import java.util.regex.Pattern;

public class StringRuleUtil {

    public static StringRule NOT_EMPTY_RULE = s -> TextUtils.isEmpty(s.toString());

    public static StringRule EMAIL_RULE = s ->
            !Patterns.EMAIL_ADDRESS.matcher(s).matches();

    public static StringRule EMPLOYEE_NAME = s ->
            !(s.length() <= 15 && Pattern.compile("[a-zA-Z\\u0621-\\u064A\\s]+[0-9]*", Pattern.CASE_INSENSITIVE).matcher(s).matches());

    public static StringRule EMPLOYEE_ABOUT = s ->
            !(s.length() <= 500 && Pattern.compile("[a-zA-Z\\u0621-\\u064A\\s]+", Pattern.CASE_INSENSITIVE).matcher(s).matches());

    public static StringRule PHONE_NUMBER = s ->
            !(Patterns.PHONE.matcher(s).matches());

    public static StringRule EMPLOYEE_SPECIALTY = s ->
            !(s.length() <= 20 && Pattern.compile("[a-zA-Z\\u0621-\\u064A\\s]+", Pattern.CASE_INSENSITIVE).matcher(s).matches());

    public static StringRule WEB_URL_RULE = s ->
            !Patterns.WEB_URL.matcher(s).matches();

    public static StringRule PASSWORD_RULE = s ->
            !Pattern.compile("^" +
                    //"(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{4,}" +               //at least 4 characters
                    "$").matcher(s).matches();

    public interface StringRule {
        boolean validate(Editable s);
    }

}
